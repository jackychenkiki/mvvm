//
//  weatherMVVMTests.swift
//  weatherMVVMTests
//
//  Created by JackyChen on 2019/6/29.
//  Copyright © 2019 labo. All rights reserved.
//

import XCTest
@testable import weatherMVVM

class weatherMVVMTests: XCTestCase {

    var loginInputViewModel: LoginInputIdViewModel!
    
    override func setUp()
    {
        loginInputViewModel = LoginInputIdViewModel()
    }

    func test_validateID_insufficient_length()
    {
        loginInputViewModel.id = DynamicType("A123456")
        XCTAssertEqual(0, loginInputViewModel.validateID.value, "位數不足")
    }
    
    func test_validateID_wrong_format()
    {
        loginInputViewModel.id = DynamicType("A12345678D")
        XCTAssertEqual(1, loginInputViewModel.validateID.value, "格式錯誤")
    }
    
    func test_validateID_correct()
    {
        loginInputViewModel.id = DynamicType("A123456789")
        XCTAssertEqual(2, loginInputViewModel.validateID.value, "格式正確")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
