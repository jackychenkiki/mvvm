//
//  weatherMVVMUITests.swift
//  weatherMVVMUITests
//
//  Created by JackyChen on 2019/6/29.
//  Copyright © 2019 labo. All rights reserved.
//

import XCTest

class weatherMVVMUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp()
    {
        continueAfterFailure = true

        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_validateID_insufficient_length()
    {
        app.buttons["SiginIn"].tap()
        app.textFields["inputId"].typeText("A123456")
        
        XCTAssertFalse(app.staticTexts["messageLabel"].exists, "警告訊息不存在")
        
        XCTAssertFalse(app.buttons["singInButton"].isEnabled, "登入按鈕呈現無作用狀態")
    }
    
    func test_validateID_wrong_format()
    {
        app.buttons["SiginIn"].tap()
        app.textFields["inputId"].typeText("A12345678D")
        
        let label = app.staticTexts["messageLabel"].label
        XCTAssertEqual("輸入錯誤，請重新輸入", label, "格式錯誤")
        
    }
    
    func test_validateID_correct()
    {
        app.buttons["SiginIn"].tap()
        app.textFields["inputId"].typeText("A123456789")
        
        XCTAssertFalse(app.staticTexts["messageLabel"].exists, "警告訊息不存在")
        
        XCTAssertTrue(app.buttons["singInButton"].isEnabled, "登入按鈕呈現作用狀態")
    }

}
