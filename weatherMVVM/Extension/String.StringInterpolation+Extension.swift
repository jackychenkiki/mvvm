//
//  String.StringInterpolation+Extension.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/5/10.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

extension String.StringInterpolation
{
    mutating func appendInterpolation<T>(_ model: CGHModel<T>)
    {
        description(model)
        
        if let data = model.data
        {
            description(data)
        }
    }
    
    mutating func description<T>(_ array: [T])
    {
        appendLiteral("\n-----  Array of \(String(describing: T.self))  -----\n\n")
        
        for object in array
        {
            mirror(object)
            
            appendLiteral("\n")
        }
        
    }
    
    mutating func description<T>(_ object: T)
    {
        appendLiteral("\n-----  \(String(describing: T.self))  -----\n\n")
        
        mirror(object)
    }
    
    mutating func mirror<T>(_ object: T)
    {
        let mirror = Mirror(reflecting: object)
        
        for (_, attr) in mirror.children.enumerated()
        {
            appendLiteral("\(attr.label!): \(attr.value)\n")
        }
    }
}
