//
//  UIColor+Extension.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/3.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

extension UIColor
{
    convenience init(red: Int, green: Int, blue: Int)
    {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int)
    {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UIColor
{
    // MARK: - Main
    
    class var cghGreen: UIColor
    {
        return UIColor(rgb: 0x00A94F)
    }
    
    class var cghWarmYellow: UIColor
    {
        return UIColor(rgb: 0xFFE316)
    }
    
    class var cghRed: UIColor
    {
        return UIColor(rgb: 0xEC4D49)
    }
    
    class var cghOrange: UIColor
    {
        return UIColor(rgb: 0xFF7313)
    }
    
    class var cghPink: UIColor
    {
        return UIColor(rgb: 0xEF5E86)
    }
    
    class var cghBlue: UIColor
    {
        return UIColor(rgb: 0x2894E8)
    }
    
    // MARK: - Scondary
    
    class var cghDarkGreen: UIColor
    {
        return UIColor(rgb: 0x0F964E)
    }
    
    class var cghLightYellow: UIColor
    {
        return UIColor(rgb: 0xFFF7BF)
    }
    
    class var cghBelgeGrey: UIColor
    {
        return UIColor(rgb: 0xF7F5F1)
    }
    
    // MARK: - Grayscale
    
    class var cghBlack: UIColor
    {
        return UIColor(rgb: 0x272727)
    }
    
    class var cghBrownishGrey: UIColor
    {
        return UIColor(rgb: 0x494949)
    }
    
    class var cghDarkGrey: UIColor
    {
        return UIColor(rgb: 0x6C6C6C)
    }
    
    class var cghWarmGrey: UIColor
    {
        return UIColor(rgb: 0x9B9B9B)
    }
    
    class var cghPinkishGrey: UIColor
    {
        return UIColor(rgb: 0xC8C8C8)
    }
    
    class var cghLightGrey: UIColor
    {
        return UIColor(rgb: 0xDDDDDD)
    }
    
    class var cghBightGrey: UIColor
    {
        return UIColor(rgb: 0xF4F4F4)
    }
    
    class var cghWhite: UIColor
    {
        return UIColor(rgb: 0xFFFFFF)
    }
}
