//
//  LoginInputIdViewController.swift
//  CathayHospital
//
//  Created by i9300609 on 2019/4/11.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class LoginInputIdViewController: UIViewController
{
    @IBOutlet weak var textFieldBgView: UIView!
    
    @IBOutlet weak var inputTextField: BindingTextField!
        {
            didSet{
                inputTextField.bind { [weak self] in guard let self = self else {return}
                    self.viewModel.id = DynamicType<String>($0, listener: self.viewModel.id.listener)
                    print("===view bind")
                }
            }
        }
    
    @IBOutlet weak var warningMsgLabel: UILabel!
    
    @IBOutlet weak var loginBtn: CornerButton!
    
    var viewModel = LoginInputIdViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputTextField.becomeFirstResponder()
        
        self.bind()
    }
}

// MARK:- Bind
extension LoginInputIdViewController
{
    func bind()
    {
        viewModel.id.bind { [weak self](id) in guard let self = self else {return}
            
            self.inputTextField.text = id
            print("===vm bind")
        }
        
        viewModel.validateID.bind {[weak self]  in guard let self = self else {return}
            
            if $0 == 0
            {
                self.loginBtn.isEnabled = false
                self.loginBtn.backgroundColor = UIColor.cghPinkishGrey
                self.textFieldBgView.layer.borderColor = UIColor.clear.cgColor
                self.textFieldBgView.layer.borderWidth = 0
                
                self.warningMsgLabel.isHidden = true
            }
            else
            {
                self.loginBtn.isEnabled = ($0 == 2)
                self.loginBtn.backgroundColor = ($0 == 2) ? UIColor.cghGreen : UIColor.cghPinkishGrey
                
                self.textFieldBgView.layer.borderColor = UIColor.cghGreen.cgColor
                self.textFieldBgView.layer.borderWidth = 1
                
                self.warningMsgLabel.isHidden = ($0 == 2)
            }
        }
    }
    
    
}

// MARK:- IBAction

extension LoginInputIdViewController
{
    @IBAction func toogleView(_ sender: Any)
    {
        self.view.endEditing(true)
    }
}

extension LoginInputIdViewController
{
    @IBAction func dataInTapped(_ sender: Any)
    {
        viewModel.id.value = "E123456789"
    }
}

extension LoginInputIdViewController: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}
