//
//  LoginInputIdViewModel.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/5/20.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import Foundation

class LoginInputIdViewModel
{
    var id: DynamicType<String>! = DynamicType("")
    {
        didSet
        {
            validateID.value = self.validateID(id.value)
        }
    }
    
    var validateID = DynamicType<Int>(0)
}

extension LoginInputIdViewModel
{
    func validateID(_ id: String)->Int
    {
        let count = id.count
        
        if count < 10
        {
            return 0
        }
        else
        {
            // 檢查格式，是否符合 開頭是英文字母＋後面9個數字
            let regex: String = "^[a-z]{1}[0-9]{9}$"
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES[c] %@", regex)
            
            if predicate.evaluate(with: id.lowercased())
            {
                return 2
            }
            else
            {
                return 1
            }
        }
    }
}
