//
//  WeatherCellTableViewCell.swift
//  weatherMVVM
//
//  Created by JackyChen on 2019/6/27.
//  Copyright © 2019 labo. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell, CellProtocol
{
    @IBOutlet var cityLabel: UILabel!
    
    @IBOutlet var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUp(viewModel: RowViewModel)
    {
        guard let viewModel = viewModel as? WeatherViewModel else
        {
            return
        }
        
        self.cityLabel.text = viewModel.city
        self.temperatureLabel.text = viewModel.temperature
    }
    
}
