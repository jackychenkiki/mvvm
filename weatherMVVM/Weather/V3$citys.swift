//
//  V2$citys.swift
//  weatherMVVM
//
//  Created by JackyChen on 2019/6/27.
//  Copyright © 2019 labo. All rights reserved.
//

import Foundation

class V3$citys: Codable
{
    var nation: String!
    var citys: [City]!
}

class City: Codable
{
    var name: String!
    
    var humidity: String!
    
    var temp: Temp!
}

class Temp: Codable
{
    var avgTemp: String!
    
    var minTemp: String!
    
    var maxTemp: String!
}

extension String.StringInterpolation
{
    mutating func appendInterpolation(_ models: [City])
    {
        description(models)
    }
    
    mutating func appendInterpolation(_ model: Temp)
    {
        description(model)
    }
}
