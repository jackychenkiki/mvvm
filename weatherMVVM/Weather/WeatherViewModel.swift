//
//  WeatherViewModel.swift
//  weatherMVVM
//
//  Created by JackyChen on 2019/6/27.
//  Copyright © 2019 labo. All rights reserved.
//

import UIKit

struct WeatherViewModel: RowViewModel
{
    var city: String!
    
    var temperature: String!
    
    init(model: City)
    {
        self.city = model.name
        self.temperature = model.temp.avgTemp
    }
}
