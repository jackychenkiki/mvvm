//
//  WeatherListViewController.swift
//  weatherMVVM
//
//  Created by JackyChen on 2019/6/27.
//  Copyright © 2019 labo. All rights reserved.
//

import UIKit

class WeatherListViewController: UIViewController
{
    @IBOutlet var tableView: UITableView!
    
    var weatherList: NSArray?
    
    var weatherViewModels: [WeatherViewModel]?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        
        fetchData()
    }
}

// MARK: - Hard code data by ViewModel

extension WeatherListViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return weatherViewModels?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeatherCell.self) , for: indexPath)
        
        let weatherViewModel = weatherViewModels![indexPath.row]
        
        if let cell = cell as? CellProtocol
        {
            cell.setUp(viewModel: weatherViewModel)
        }
        
        return cell
    }
    
    // MARK: - if service API is not ready yet
//    func fetchData()
//    {
//        weatherViewModels = [WeatherViewModel(city: "Taipei", temperature: "29"),
//                             WeatherViewModel(city: "Kaohsiung", temperature: "30")]
//    }
    
    // MARK: - if service API is ready 
    func fetchData()
    {
        ServiceAPI<V3$citys>.GET(parameters: nil) { [weak self](result) in guard let self = self else{return}

            switch result
            {
                case .success(let model):
                    
                    print("\(model)")
                    print("\(model.data!.citys!)")
                    print("\(model.data!.citys![0].temp!)")

                    self.weatherViewModels = model.data?.citys.map({ (city) -> WeatherViewModel in
                        return WeatherViewModel(model: city)
                    })

                    self.tableView.reloadData()

                case .failure(let error):
                    print(error.localizedDescription)

            }
        }
    }
}

// MARK: - Hard code data by Dictionary

//extension WeatherListViewController: UITableViewDataSource
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return weatherList?.count ?? 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeatherCell.self) , for: indexPath) as! WeatherCell
//
//        let dict = weatherList![indexPath.row] as! NSDictionary
//
//        cell.cityLabel.text = dict["city"] as! String
//        cell.temperatureLabel.text = (dict["temp"] as! NSDictionary)["avgTemp"] as! String
//
//        return cell
//    }
//
//    func fetchData()
//    {
//        let result = """
//                    [
//                        {
//                        "city":"Taipei",
//                            "temp":{
//                                "avgTemp":"29",
//                                "minTemp":"23",
//                                "maxTemp":"33"
//                            },
//                            "humidity":"30%"
//                        },
//                        {
//                        "city":"Kaohsiung",
//                            "temp":{
//                                "avgTemp":"30",
//                                "minTemp":"25",
//                                "maxTemp":"36"
//                        },
//                        "humidity":"20%"
//                        }
//                    ]
//                    """
//
//        let data = result.data(using: String.Encoding.utf8)
//
//        weatherList = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSArray
//
//        print("=====weatherList: \(weatherList)")
//    }
//}
