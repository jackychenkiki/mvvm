//
//  CGHServiceAPI.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/22.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class ServiceAPI<T: Codable>
{
    typealias callback<T: Codable> = (Swift.Result<CGHModel<T>, Error>) -> Void
    
    class func GET(parameters: [String: Any]? = nil,
                   callback: callback<T>? = nil)
    {
        self.request(method: .get,
                     parameters: parameters,
                     callback: callback)
    }
    
    class func POST(parameters: [String: Any]? = nil,
                   callback: callback<T>? = nil)
    {
        self.request(method: .post,
                     parameters: parameters,
                     callback: callback)
    }
    
    class func request( method: Alamofire.HTTPMethod,
                        parameters: [String: Any]? = nil,
                        callback: callback<T>? = nil )
    {
        let className = String(describing: T.self).lowercased()
        
        let pathArray = className.split(separator: "$")
        
        var urlString: String = Environment.host
        
        // ToDo : for test
//        urlString = "https://www.google.com"
        
        for path in pathArray
        {
            urlString.append("/" + path)
        }
        
        Alamofire.request(urlString,
                          method: method,
                          parameters: parameters)
            .responseData
            { (response) in
                
//                let str = String(data: response.data!, encoding: .utf8)
//                print("====str: \(str)")
                
                guard response.result.isSuccess else
                {
                    callback?(.failure(response.error!))
                    return
                }
                
                DispatchQueue.main.async
                {
                    do
                    {
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .iso8601
                        let model = try decoder.decode(CGHModel<T>.self, from: response.data!)
                        
                        callback?(.success(model))
                    }
                    catch
                    {
                        callback?(.failure(ProcessError.decodeError))
                    }
                }
            }
        
    }// end func
}
