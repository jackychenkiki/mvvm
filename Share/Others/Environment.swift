//
//  Environment.swift
//  CathayHospital
//
//  Created by 蔡欣穎 on 2018/12/28.
//  Copyright © 2018 CathayHospital. All rights reserved.
//

import Foundation

enum EnvironmentType
{
    case debug, local, gdtest, release
}

struct Environment
{
    static var host: String
    {
        return "https://k162es96j3vi9uqzv0ikqa-on.drv.tw/treelife"
    }
}
