//
//  Protocol.swift
//  weatherMVVM
//
//  Created by JackyChen on 2019/6/28.
//  Copyright © 2019 labo. All rights reserved.
//

import Foundation

protocol CellProtocol
{
    func setUp(viewModel: RowViewModel)
}

protocol RowViewModel {}
