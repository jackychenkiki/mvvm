//
//  CGHError.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/25.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import Foundation

public enum ProcessError: Int, Error
{
    case decodeError = -20010
}

extension ProcessError: LocalizedError
{
    public var errorDescription: String?
    {
        switch self
        {
            case .decodeError:
                return NSLocalizedString("伺服器回傳無效的格式", comment: "無效的JSON格式")
        }
    }
}
