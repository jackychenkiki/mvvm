//
//  BindingTextField.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/31.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class BindingTextField: UITextField {
    
    typealias Listener = (String)->Void
    
    var listener: Listener!
    
    func bind(listener: Listener!)
    {
        self.listener = listener
        
        self.addTarget(self, action: #selector(BindingTextField.textChange), for: .editingChanged)
    }
    
    @objc
    func textChange(_ textField: UITextField)
    {
        guard let text = textField.text else {return}
        self.listener(text)
    }
}
