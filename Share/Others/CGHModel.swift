//
//  CGHModel.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/24.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import Foundation

class CGHModel<T: Codable>: Codable
{
    var rtnCode: String!
    var data: T?
}
