//
//  Enum.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/25.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import Foundation

// MARK: - AlertImage

enum AlertImage: String
{
    case icCelebrate = "icCelebrate"
    
    case icAddAccount = "icAddAccount"
    
    case icBusy = "icBusy"
    
    case icError = "icError"
    
    case icNoRecord = "icNoRecord"
    
    case icWelcomeLogin = "icWelcomeLogin"
    
    case icWifi = "icWifi"
    
    case icWrongPhone = "icWrongPhone"
}

// MARK: - ButtonArrange

enum ButtonArrange
{
    case Horizontal
    case Vertical
}
