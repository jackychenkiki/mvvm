//
//  DynamicType.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/5/20.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import Foundation

class DynamicType<T>
{
    typealias Listener = (T)->Void
    
    var listener: Listener?
    
    var value: T
    {
        didSet
        {
            listener?(value)
        }
    }
    
    func bind(listener: @escaping Listener)
    {
        self.listener = listener
        self.listener?(value)
    }
    
    init(_ value: T, listener: Listener? = nil) {
        self.value = value
        self.listener = listener
    }
}
