//
//  CornerButton.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/14.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class CornerButton: UIButton
{
    typealias Callback = ()->Void
    
    var callback: Callback?
    
    convenience init(title: String,
                     titleColor: UIColor? = UIColor.cghWhite,
                     backgroundColor: UIColor,
                     callback: Callback? = nil)
    {
        self.init()
        
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
        self.callback = callback
        self.backgroundColor = backgroundColor
        
        setUI()
        
        self.addTarget(self, action: #selector(CornerButton.click), for: .touchUpInside)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        setUI()
    }
}

extension CornerButton
{
    @objc func click()
    {
        UIApplication.topViewController()?.dismiss(animated: false)
        {
            if let callback = self.callback
            {
                callback()
            }
        }
    }
    
    fileprivate func setUI()
    {
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
        
        self.translatesAutoresizingMaskIntoConstraints = false
        let heightConstraint = self.heightAnchor.constraint(equalToConstant: 48)
        heightConstraint.priority = UILayoutPriority(rawValue: 251)
        heightConstraint.isActive = true
    }
}
