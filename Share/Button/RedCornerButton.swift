//
//  RedCornerButton.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/22.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class RedCornerButton: CornerButton
{
    convenience init(title: String, callback: Callback? = nil)
    {
        self.init(title: title, backgroundColor: UIColor.cghRed, callback: callback)
    }
}
