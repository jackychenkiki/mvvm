//
//  WhiteCornerButton.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/22.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class WhiteCornerButton: CornerButton
{
    convenience init(title: String,
                     titleColor: UIColor,
                     callback: Callback? = nil)
    {
        self.init(title: title,
                  titleColor: titleColor,
                  backgroundColor: UIColor.cghWhite,
                  callback: callback)
    }
}
