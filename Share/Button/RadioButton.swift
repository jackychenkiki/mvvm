//
//  RadioButton.swift
//  CathayHospital
//
//  Created by i9300609 on 2019/4/11.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class RadioButton: UIButton {

    var isChecked = false
    {
        didSet
        {
            self.setImage(UIImage(named: self.isChecked ? "icRadioP20" : "icRadioN20"), for: .normal)
        }
    }

}
