//
//  GreenCornerButton.swift
//  CathayHospital
//
//  Created by JackyChen on 2019/1/17.
//  Copyright © 2019 CathayHospital. All rights reserved.
//

import UIKit

class GreenCornerButton: CornerButton
{
    convenience init(title: String, callback: Callback? = nil)
    {
        self.init(title: title, backgroundColor: UIColor.cghGreen, callback: callback)
    }
}
